package ebay.glue;

import cucumber.api.java.After;
import ebay.pages.Page;

public class Hooks extends Page {

    @After
    public void afterScenario() {
        driver.quit();
        driver = null;
    }
}
