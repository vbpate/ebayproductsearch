package ebay.glue;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import ebay.steps.BikeSearchSteps;
import net.thucydides.core.annotations.Steps;

public class BikeGlue {

    @Steps
    BikeSearchSteps bikeSearchSteps;

    @Given("^User is on Ebay website$")
    public void userIsOnEbayWebsite() {
        bikeSearchSteps.openHomePage();
    }

    @When("^User search for a \"([^\"]*)\"$")
    public void user_search_for_a(String searchProduct) {
        bikeSearchSteps.searchProduct(searchProduct);
    }

    @And("^Add first bike into cart$")
    public void addFirstBikeIntoCart() {
        bikeSearchSteps.addProductToCart();
    }

    @Then("^Item should display in cart$")
    public void itemShouldDisplayInCart() {
        bikeSearchSteps.verifyproductincart();
    }


    @When("^User click on Continue button on My Ebay without providing user details$")
    public void user_click_on_Continue_button_without_providing_user_details() {
        bikeSearchSteps.openMyeBay();
    }

    @Then("^User should get error message as 'Oops, that's not a match'$")
    public void user_should_get_error_message_as_Oops_that_s_not_a_match() {

    }
}
