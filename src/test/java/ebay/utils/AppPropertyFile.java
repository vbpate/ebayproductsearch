package ebay.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class AppPropertyFile {

    /**
     * This class is used to read data from the property file
     * */


    public static Properties prop;

    static {
        InputStream resourceStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("app.properties");
        prop = new Properties();
        try {
            prop.load(resourceStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
