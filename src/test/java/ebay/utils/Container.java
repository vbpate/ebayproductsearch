package ebay.utils;

import java.util.HashMap;
import java.util.Map;

public class Container {
    /**
     * This class is used to store and share the data between cucumber steps
     * */

    private Map<String, Object> map;

    public Container(Map<String, Object> map) {
        if (map.size() == 0) {
            this.map = new HashMap<String, Object>();
        }
    }

    public <T> void store(String key, T value) {
        map.put(key, value);
    }

    public <T> T get(String key) {
        return (T) map.get(key);
    }
}
