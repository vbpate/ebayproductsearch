package ebay.steps;

import ebay.pages.*;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class BikeSearchSteps extends ScenarioSteps {
    EBayHomePage eBayHomePage;
    EBaySearchResultPage eBaySearchResultPage;
    EBayProductPage eBayProductPage;
    EBayCartPage eBayCartPage;
    EBayLoginPage eBayLoginPage;

    @Step
    public void openHomePage() {
        eBayHomePage = new EBayHomePage();
        eBayHomePage.getebayHomePage();
    }

    @Step
    public void searchProduct(String seachProduct) {
        eBayHomePage.sendKeyinProductTextbox(seachProduct);
        eBaySearchResultPage = eBayHomePage.clickSearchButton();
    }

    @Step
    public void addProductToCart() {
        eBayProductPage = eBaySearchResultPage.clickFirstSearchLink();
        eBayProductPage.setFirstSelectionValue();
        eBayProductPage.getProductName();
        eBayProductPage.clickAddtoCartButton();
        eBayCartPage = eBayProductPage.clickNothanksButton();
    }

    @Step
    public void verifyproductincart() {
        eBayCartPage.verifyProductName();
    }

    public void openMyeBay() {
        eBayLoginPage = eBayHomePage.clickMyeBay();
        eBayLoginPage.clickContinueButton();
    }

    public void verifySignInError() {
        eBayLoginPage.verifySiginErrormsg();
    }
}
