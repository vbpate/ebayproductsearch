package ebay.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class EBaySearchResultPage extends Page {

    private WebElement getfirstSearchLinkElement() {
        return driver.findElement(By.xpath("//div[@id='srp-river-main']//li[contains(@data-view,'iid:1')]//a"));
    }

    public EBayProductPage clickFirstSearchLink() {
        getfirstSearchLinkElement().click();
        return new EBayProductPage();
    }
}
