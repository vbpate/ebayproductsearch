package ebay.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class EBayLoginPage extends Page {

    private WebElement getContinueButton() {
        return driver.findElement(By.id("signin-continue-btn"));
    }

    private WebElement getSigninErrortag() {
        return driver.findElement(By.id("signin-error-msg"));
    }

    public void clickContinueButton() {
        getContinueButton().click();
    }

    public void verifySiginErrormsg() {
        Assert.assertEquals("Oops, that's not a match.", getSigninErrortag().getText());
    }
}
