package ebay.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class EBayProductPage extends Page {

    private WebElement getSelectionDropdown() {
        return driver.findElement(By.name("Colour"));
    }

    private WebElement getNothanksButton() {
        return driver.findElement(By.xpath("//div[@id='ADDON_0']//button[contains(@class,'addonnothx ')]"));
    }

    private WebElement getProductTitle() {
        return driver.findElement(By.id("itemTitle"));
    }

    private WebElement getAddtoCartButton() {
        return driver.findElement(By.id("isCartBtn_btn"));
    }

    public void setFirstSelectionValue() {
        new Select(getSelectionDropdown()).selectByIndex(1);
    }

    public String getProductName() {
        container.store("productName", getProductTitle().getText());
        return getProductTitle().getText();
    }

    public void clickAddtoCartButton() {
        getAddtoCartButton().click();
    }

    public EBayCartPage clickNothanksButton() {
        getNothanksButton().click();
        return new EBayCartPage();
    }
}
