package ebay.pages;

import ebay.utils.AppPropertyFile;
import ebay.utils.Container;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;

public class Page {
    protected static Container container;
    protected static WebDriver driver;
    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    public Page() {
        if (container == null) {
            container = new Container(new HashMap<String, Object>());
        }
        if (driver == null) {
            if (AppPropertyFile.prop.getProperty("browser").equalsIgnoreCase("chrome")) {
                WebDriverManager.chromedriver().browserVersion(AppPropertyFile.prop.getProperty("chrome-version")).setup();
                ChromeOptions options = new ChromeOptions();
                options.addArguments("start-maximized");
                options.addArguments("enable-automation");
                options.addArguments("--no-sandbox");
                options.addArguments("--disable-infobars");
                options.addArguments("--disable-dev-shm-usage");
                options.addArguments("--disable-browser-side-navigation");
                options.addArguments("--disable-gpu");
                driver = new ChromeDriver(options);
            }
        }
    }

}
