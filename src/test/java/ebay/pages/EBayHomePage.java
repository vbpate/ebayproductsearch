package ebay.pages;

import ebay.utils.AppPropertyFile;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class EBayHomePage extends Page {

    public void getebayHomePage() {
        driver.get(AppPropertyFile.prop.getProperty("URL"));
    }

    private WebElement getSeachTextbox() {
        return driver.findElement(By.id("gh-ac"));
    }

    private WebElement getSeachButton() {
        return driver.findElement(By.id("gh-btn"));
    }

    private WebElement getMyEbayTitle() {
        return driver.findElement(By.xpath("//a[text()='My eBay']"));
    }

    public void sendKeyinProductTextbox(String product) {
        getSeachTextbox().sendKeys(product);
    }

    public EBaySearchResultPage clickSearchButton() {
        getSeachButton().click();
        return new EBaySearchResultPage();
    }

    public EBayLoginPage clickMyeBay() {
        getMyEbayTitle().click();
        return new EBayLoginPage();
    }
}
