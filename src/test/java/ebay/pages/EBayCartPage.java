package ebay.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class EBayCartPage extends Page {

    private WebElement getProductTitleElement() {
        return driver.findElement(By.xpath("//a[@data-test-id='cart-item-link']"));
    }

    public String getProductName() {
        return getProductTitleElement().getText();
    }

    public void verifyProductName() {
        Assert.assertEquals(container.get("productName"), getProductName());
    }
}
