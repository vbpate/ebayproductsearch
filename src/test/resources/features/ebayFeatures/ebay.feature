Feature: test

  @productSearch
  Scenario: Verify that searched bike added to the cart
    Given User is on Ebay website
    When User search for a "bike"
    And Add first bike into cart
    Then Item should display in cart

  @loginverification
  Scenario: Verify error message when user has not provided sign in details
    Given User is on Ebay website
    When User click on Continue button on My Ebay without providing user details
    Then User should get error message as 'Oops, that's not a match'