### This project demonstrating search criteria and error message verification 
The project directory structure :

- src
    + test 
    + java - Test runners and supporting code
    + resources
      + features - Feature files 
     
- To execute tests:
 /gradlew clean test aggregate

- Output directory:
 build/site/serenity/index.html
    - This has a human readable pie chart showing the result of the test run
    - One can dril down into individual tests and steps of those tests.
      
